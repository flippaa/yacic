#ifndef YACIC_JPEGREADER_H
#define YACIC_JPEGREADER_H
#include "bitmapReader.h"
extern "C" {
#include <jpeglib.h>
}

class JPEGReader : public Image {
public:
    JPEGReader(FILE *_FD) : Image(_FD) {
        struct jpeg_decompress_struct cinfo;
        struct jpeg_error_mgr jerr;
        cinfo.err = jpeg_std_error (&jerr);
        jerr.error_exit = jpegErrorExit;
        jpeg_create_decompress( &cinfo );
        jpeg_stdio_src(&cinfo, FD);
        auto err = jpeg_read_header(&cinfo, TRUE);

        try {
            jpeg_start_decompress(&cinfo);
            width = cinfo.image_width;
            height = cinfo.image_height;

            unsigned long location = 0;
            raw_image = (unsigned char *) malloc(
                    cinfo.output_width * cinfo.output_height * 3); // FLAW (if components number is more than 3)
            // cinfo.num_components);
            JSAMPROW row_pointer[1];
            row_pointer[0] = (unsigned char *) malloc(cinfo.output_width * 3);
            while (cinfo.output_scanline < cinfo.image_height) {
               jpeg_read_scanlines(&cinfo, row_pointer, 1);
                for (auto i = 0; i < cinfo.image_width * 3; i++)
                    raw_image[location++] = row_pointer[0][i];
            }
        }
        catch ( ... ) {
            throw;
        }
    };
    virtual ~JPEGReader() {};
    virtual unsigned char* getPixelArray() override {
        return raw_image;
    }
    virtual std::pair<double, double> getCG() override {

    };
    virtual double getRMSE() override {

    };
private:
    static void jpegErrorExit ( j_common_ptr cinfo )
    {
        char jpegLastErrorMsg[JMSG_LENGTH_MAX];
        ( *( cinfo->err->format_message ) ) ( cinfo, jpegLastErrorMsg );
        throw std::runtime_error( jpegLastErrorMsg );
    }
};

#endif //YACIC_JPEG_H
