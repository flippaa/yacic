#pragma once
#include "iostream"

class Image {
public:
    Image(FILE *FD);
    virtual ~Image();
    virtual unsigned char* getPixelArray() = 0;
    virtual std::pair<double, double> getCG() = 0;
    virtual double getRMSE() = 0;
    int height;
    int width;
    std::string getFilename();
    static Image* getReader(const std::string& );
protected:
    unsigned char* raw_image;
    FILE *FD;
};

class ReaderException{
public:
    ReaderException(const std::string& str) : msg(str) {};
    void what(){
        std::cout<< msg <<std::endl;
    }
private:
    std::string msg;
};