#include <iostream>
#include "jpegReader.h"
#include "pngReader.h"
#include "exception"

int WIDTH_THRESHOLD = 6;
int HEIGHT_THRESHOLD = 6;
int RMSERROR_THRESHOLD = 8;
/*
getPixArrayFromRect( rect ) {

        var sub = Image.getSubimage( rect[0], rect[1], rect[2], rect[3] );
        return sub.getRGB(0, 0, rect[2], rect[3], null, 0, rect[2]);
}

bool isDivisible(int rect[4] ) {

    if (rect[2] < WIDTH_THRESHOLD || rect[3] < HEIGHT_THRESHOLD)
                return false;

    auto pixArray = getPixArrayFromRect( rect );
    auto rms = Packages.ImageUtilities.getRMSError( pixArray );

    if (rms < RMSERROR_THRESHOLD)
        return false;

    return true;
}

void  quadRecurse(char* arr, int rect[4] ) {
    if ( !isDivisible( rect ) ) {
        arr.push( rect );
        return;
    }

    var newRects = quadDivide( rect ); // partition rect

    for (var i = 0; i < newRects.length; i++)  // size check
        if (newRects[i][2] < 1 || newRects[i][3] < 1) {
            arr.push(rect);
            return;
        }

    for (auto i = 0; i < newRects.length; i++) // recurse on each new rect
        quadRecurse( arr, newRects[ i ] );
}
}
*/

Image* Image::getReader(const std::string& fname) {
    FILE *infile;
    if ((infile = fopen(fname.c_str(), "rb")) == NULL) {
        std::cout << "Can't open " << fname << std::endl;
        exit(1);
    }

    try {
        JPEGReader* jp = new JPEGReader(infile);
        fclose( infile );
        return jp;
    }
    catch ( ... ) {
        fclose( infile );
        exit(2);
    }
}

unsigned char *raw_image = NULL;

int main(int argc, char *argv[]) {
    auto reader = Image::getReader("/home/l/CLionProjects/yacic/example.jpeg");
    int rect[] = { 0, 0 , reader->height, reader->width };
    auto raw_image = (char*)malloc(reader->height * reader->width * 3);  // FLAW if components number more than 3
    // cinfo.num_components);


    return 0;
}