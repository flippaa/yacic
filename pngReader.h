#ifndef YACIC_PNGREADER_H
#define YACIC_PNGREADER_H
#include "bitmapReader.h"

class PNGReader : public Image {
public:
    PNGReader(FILE *_FD) : Image(_FD) {};
    virtual ~PNGReader() {};
    virtual unsigned char* getPixelArray() override {
    }
    virtual std::pair<double, double> getCG() override {
    };
    virtual double getRMSE() override {
    };
};

#endif //YACIC_PNGREADER_H
